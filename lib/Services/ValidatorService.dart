class ValidatorService {

  static final ValidatorService _validatorService = new ValidatorService
      ._internal();

  factory ValidatorService() {
    return _validatorService;
  }
  ValidatorService._internal();

  String validateName(String value) {
    if (value.length < 1) {
      return 'Un nom doit obligatoirement être donné';
    }

    return null;
  }

}