import 'package:where_is_my_wood_pile/Modeles/WoodPile.dart';
import 'package:where_is_my_wood_pile/Repository/WoodPileRepository.dart';

class WoodPileService {

  static final WoodPileService _woodPileService = new WoodPileService._internal();
  WoodPileRepository _woodPileRepository = new WoodPileRepository();

  factory WoodPileService() {
    return _woodPileService;
  }
  WoodPileService._internal();

  List<WoodPile> GetAllWoodPiles(){
    return <WoodPile>[
      new WoodPile('Tas de bois 1', 'Mon super tas de bois 1', 12, 15, 13.5, 'path'),
      new WoodPile('Tas de bois 2', 'Mon super tas de bois 2', 12, 15, 13.5, 'path'),
      new WoodPile('Tas de bois 3', 'Mon super tas de bois 3', 12, 15, 13.5, 'path'),
      new WoodPile('Tas de bois 4', 'Mon super tas de bois 4', 12, 15, 13.5, 'path'),
      new WoodPile('Tas de bois 5', 'Mon super tas de bois 5', 12, 15, 13.5, 'path'),
      new WoodPile('Tas de bois 6', 'Mon super tas de bois 6', 12, 15, 13.5, 'path'),];
  }
}