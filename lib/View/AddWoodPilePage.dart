import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:where_is_my_wood_pile/Modeles/WoodPile.dart';
import 'package:where_is_my_wood_pile/Services/ValidatorService.dart';
import 'package:image_picker/image_picker.dart';

class AddWoodPilePage extends StatefulWidget {
  @override
  _AddWoodPilePageState createState() => _AddWoodPilePageState();
}

class _AddWoodPilePageState extends State<AddWoodPilePage> with SingleTickerProviderStateMixin {
  AnimationController _controller;


  final _formKey = GlobalKey<FormState>();
  final _validatorService = new ValidatorService();

  WoodPile _woodPile = new WoodPile.empty();

  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }

  @override
  void initState() {
    _controller = AnimationController(vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        tooltip: 'Photo',
        child: Icon(Icons.add_a_photo),
        onPressed: () {
          getImage();
        },
      ),
      appBar: AppBar(
        title: Text('Ajouter un tas de bois'),
        actions: <Widget>[
          IconButton(
            icon :Icon(Icons.check),
            onPressed: () {
              // Validate will return true if the form is valid, or false if
              // the form is invalid.
              if (_formKey.currentState.validate()) {
                // If the form is valid, we want to show a Snackbar
                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text('Processing Data')));
              }
            },
          ),
        ],
      ),
      body: BuildAddWoodPileForms(),

    );
  }

  Widget BuildAddWoodPileForms() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Center(
                child: _image == null
                ? Text('No image selected.')
                  : Image.file(_image),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Nom',
                      filled: true,
                      fillColor: Colors.grey[300],
                    ),
                    keyboardType: TextInputType.text,
                    validator: _validatorService.validateName,
                    onSaved: (String value) {
                      this._woodPile.name = value;
                    }
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                    decoration: InputDecoration(
                        labelText: 'Description',
                      filled: true,
                      fillColor: Colors.grey[300],
                    ),
                    onSaved: (String value) {
                      this._woodPile.description = value;
                    }
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Volume',
                      filled: true,
                      fillColor: Colors.grey[300],
                    ),
                    keyboardType: TextInputType.number,
                    onSaved: (String value) {
                      this._woodPile.volume = double.parse(value);
                    }
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children : <Widget>[
                    Flexible(
                    child : TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Latitude',
                          filled: true,
                          fillColor: Colors.grey[300],
                        ),
                        keyboardType: TextInputType.number,
                        onSaved: (String value) {
                          this._woodPile.latitude = double.parse(value);
                        }
                      ),
                    ),
                    VerticalDivider (
                      color : Colors.deepOrange,
                    ),
                    Flexible(
                    child : TextFormField(
                        decoration: InputDecoration(
                            labelText: 'Longitude',
                          filled: true,
                          fillColor: Colors.grey[300],
                        ),
                        keyboardType: TextInputType.number,
                        onSaved: (String value) {
                          this._woodPile.longitude = double.parse(value);
                        }
                      ),
                    )
                  ]
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
