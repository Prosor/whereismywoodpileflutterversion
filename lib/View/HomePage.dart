import 'package:flutter/material.dart';
import 'package:where_is_my_wood_pile/Modeles/WoodPile.dart';
import 'package:where_is_my_wood_pile/Services/WoodPileService.dart';
import 'package:where_is_my_wood_pile/View/AddWoodPilePage.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  static final WoodPileService _woodPileService = new WoodPileService();
  final List<WoodPile> _woodPileList = _woodPileService.GetAllWoodPiles();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: BuildWoodPileList(),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Ajouter',
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AddWoodPilePage()),
          );
        },
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget BuildWoodPileList(){
    return ListView.separated(
        separatorBuilder: (context, index) => Divider(),
        itemCount: _woodPileList.length,
        padding: const EdgeInsets.all(16.0),
        itemBuilder: /*1*/ (context, index) {
          return BuildCard(_woodPileList[index]);
        });
  }

  Widget BuildCard(WoodPile woodPile){
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            //leading: Icon(Icons.pin_drop),
            leading : new Image.asset(
              'images/wood_pile.jpg',
              height: 60.0,
              fit: BoxFit.cover,
            ),
            title: Text(woodPile.name),
            subtitle: Text(woodPile.getStringDepositDate()),
          ),
          Align(
              alignment: Alignment(-0.11, 0),
              child : Text('Volume : '+woodPile.volume.toString()+'st')
          ),
          ButtonTheme.bar( // make buttons use the appropriate styles for cards
            child: ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: const Text('MAPS'),
                  onPressed:(){ /*...*/},
                ),
                FlatButton(
                  child: const Text('VOIR'),
                  onPressed: () { /* ... */ },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}