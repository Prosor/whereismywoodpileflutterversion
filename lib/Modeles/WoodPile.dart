
import "package:intl/intl.dart";

class WoodPile {
  String name;
  String description;
  double latitude;
  double longitude;
  double volume;
  String picturePath;
  DateTime _depositDate;

  WoodPile.empty()
  {
    this._depositDate = DateTime.now();
  }

  WoodPile(this.name, this.description, this.latitude, this.longitude, this.volume, this.picturePath)
  {
    this._depositDate = DateTime.now();
  }


  String getStringDepositDate(){
    var format = new DateFormat("d.MM.y");
    return format.format(new DateTime.now()).toString();
  }



}